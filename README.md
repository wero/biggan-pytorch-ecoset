# BigGAN-PyTorch-ecoset

Here are code, weights, and samples for "Training BigGAN on an ecologically motivated image dataset" by Weronika Kłos, Piero Coronica, Katja Seeliger and Martin N. Hebart. Our code is a modified version of [BigGAN-PyTorch](https://github.com/ajbrock/BigGAN-PyTorch) by Andy Brock and Alex Andonian. Please also refer to their original `README.md` contained in the `code` folder, as most of the useful information is already mentioned there.


## User Manual

You will need:

- [PyTorch](https://PyTorch.org/), version 1.8.1
- [Horovod](https://github.com/horovod/horovod)
- Open MPI 4.0.0
- tqdm, numpy, scipy, and h5py
- the [ecoset](https://codeocean.com/capsule/9570390/tree/v1) dataset (for training only)

For traning you should first run the `prepare_ecoset.sh` script. The `--data_root` parameter needs to be set to the location of your data. Please note, that the subdirectories of `--data_root` are assumed to correspond to classes, hence you will need to either merge the train, val and test directories of ecoset into one directory or specify only train as the `--data_root`.

We also included two slurm batch scripts:

- `train.sh`: The script that was used for our most successful training run. It is the same one that produced the weights you can find in the `weights` folder and the samples in the `samples` folder. We trained on 10 nodes with 4 Nvidia A100 GPUs each.

- `sample.sh`: This is used for inference on an already trained model, eg. to test FID/IS at different truncation values, produce more samples or some interpolations, and does not require as many computational resources as the training script (one GPU is enough, but you might need to adjust the batch size).

By default, the IS and FID values obtained are based on the commonly used Inception Net pretrained on ImageNet. If you are interested in our ecoIS values instead, you can append the parameter `--use_ecoIS`. We recommend using the weights with the suffix "best2", which in the case of our scripts is done by passing the arguments `--resume` and `--load_weights best2`. These correspond to the checkpoint taken immediately before collapse.


## Metrics

A key-feature of BigGANs is, that it is possible to trade off variety vs. fidelity of generated images by sampling from a truncated gaussian during inference. When computing the metrics, this leads to a trade-off between IS and FID. Below you can see the FID-IS curve (of the model shared in this repository) obtained by using different degrees of truncation, i.e. different values for the variance of the latent vector z. It is common practice to report these values, but we did not have the space to report them in the original publication due to the page limit.

![Truncation Curve](code/imgs/trunc_plot.jpg?raw=true "Truncation Curve")

Feel free to reach out if you have any questions or need help!